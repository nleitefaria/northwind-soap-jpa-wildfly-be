package com.mycompany.myapp.dao.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.mycompany.myapp.dao.CategoriesDAO;
import com.mycompany.myapp.dao.impl.exceptions.NonexistentEntityException;
import com.mycompany.myapp.dao.impl.exceptions.PreexistingEntityException;
import com.mycompany.myapp.entity.Categories;
import com.mycompany.myapp.entity.Products;

public class CategoriesDAOImpl implements CategoriesDAO, Serializable {

    public CategoriesDAOImpl(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Categories categories) throws PreexistingEntityException, Exception {
        if (categories.getProductsList() == null) {
            categories.setProductsList(new ArrayList<Products>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Products> attachedProductsList = new ArrayList<Products>();
            for (Products productsListProductsToAttach : categories.getProductsList()) {
                productsListProductsToAttach = em.getReference(productsListProductsToAttach.getClass(), productsListProductsToAttach.getProductID());
                attachedProductsList.add(productsListProductsToAttach);
            }
            categories.setProductsList(attachedProductsList);
            em.persist(categories);
            for (Products productsListProducts : categories.getProductsList()) {
                Categories oldCategoryIDOfProductsListProducts = productsListProducts.getCategoryID();
                productsListProducts.setCategoryID(categories);
                productsListProducts = em.merge(productsListProducts);
                if (oldCategoryIDOfProductsListProducts != null) {
                    oldCategoryIDOfProductsListProducts.getProductsList().remove(productsListProducts);
                    oldCategoryIDOfProductsListProducts = em.merge(oldCategoryIDOfProductsListProducts);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findCategories(categories.getCategoryID()) != null) {
                throw new PreexistingEntityException("Categories " + categories + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Categories categories) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Categories persistentCategories = em.find(Categories.class, categories.getCategoryID());
            List<Products> productsListOld = persistentCategories.getProductsList();
            List<Products> productsListNew = categories.getProductsList();
            List<Products> attachedProductsListNew = new ArrayList<Products>();
            for (Products productsListNewProductsToAttach : productsListNew) {
                productsListNewProductsToAttach = em.getReference(productsListNewProductsToAttach.getClass(), productsListNewProductsToAttach.getProductID());
                attachedProductsListNew.add(productsListNewProductsToAttach);
            }
            productsListNew = attachedProductsListNew;
            categories.setProductsList(productsListNew);
            categories = em.merge(categories);
            for (Products productsListOldProducts : productsListOld) {
                if (!productsListNew.contains(productsListOldProducts)) {
                    productsListOldProducts.setCategoryID(null);
                    productsListOldProducts = em.merge(productsListOldProducts);
                }
            }
            for (Products productsListNewProducts : productsListNew) {
                if (!productsListOld.contains(productsListNewProducts)) {
                    Categories oldCategoryIDOfProductsListNewProducts = productsListNewProducts.getCategoryID();
                    productsListNewProducts.setCategoryID(categories);
                    productsListNewProducts = em.merge(productsListNewProducts);
                    if (oldCategoryIDOfProductsListNewProducts != null && !oldCategoryIDOfProductsListNewProducts.equals(categories)) {
                        oldCategoryIDOfProductsListNewProducts.getProductsList().remove(productsListNewProducts);
                        oldCategoryIDOfProductsListNewProducts = em.merge(oldCategoryIDOfProductsListNewProducts);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = categories.getCategoryID();
                if (findCategories(id) == null) {
                    throw new NonexistentEntityException("The categories with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Categories categories;
            try {
                categories = em.getReference(Categories.class, id);
                categories.getCategoryID();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The categories with id " + id + " no longer exists.", enfe);
            }
            List<Products> productsList = categories.getProductsList();
            for (Products productsListProducts : productsList) {
                productsListProducts.setCategoryID(null);
                productsListProducts = em.merge(productsListProducts);
            }
            em.remove(categories);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Categories> findCategoriesEntities() {
        return findCategoriesEntities(true, -1, -1);
    }

    public List<Categories> findCategoriesEntities(int maxResults, int firstResult) {
        return findCategoriesEntities(false, maxResults, firstResult);
    }

    private List<Categories> findCategoriesEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Categories.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Categories findCategories(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Categories.class, id);
        } finally {
            em.close();
        }
    }

    public int getCategoriesCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Categories> rt = cq.from(Categories.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
