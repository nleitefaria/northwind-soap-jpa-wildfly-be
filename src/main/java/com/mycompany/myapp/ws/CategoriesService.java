package com.mycompany.myapp.ws;

import javax.jws.WebMethod;
import javax.jws.WebService;

import com.mycompany.myapp.manager.CategoriesManager;
import com.mycompany.myapp.manager.impl.CategoriesManagerImpl;

@WebService
public class CategoriesService 
{	
	private  CategoriesManager categoriesManager;
	
	public CategoriesService()
    {
		categoriesManager = new CategoriesManagerImpl();
    }
    
    @WebMethod
    public Integer getCategoriesCount()
    {  	
        return categoriesManager.getCategoriesCount();
    }

}
