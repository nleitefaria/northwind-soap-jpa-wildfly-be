package com.mycompany.myapp.manager.impl;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.mycompany.myapp.dao.CategoriesDAO;
import com.mycompany.myapp.dao.impl.CategoriesDAOImpl;
import com.mycompany.myapp.manager.CategoriesManager;

public class CategoriesManagerImpl implements CategoriesManager 
{
	public  CategoriesManagerImpl()
	{
	}

	public Integer getCategoriesCount()
    { 
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("JPASQLServerPU");   
		CategoriesDAO categoriesDao = new CategoriesDAOImpl(emf);      
        return categoriesDao.getCategoriesCount();
    }

}
