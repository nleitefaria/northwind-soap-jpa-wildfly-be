SOAP Webservices over the sample Northwind database using:

- SOAP
- Wildfly
- JavaEE
- JPA
- SQLServer

![picture](http://do.eg.mahidol.ac.th/services2/demos/northwind-db/assets/edf0d1ed/northwind.gif)